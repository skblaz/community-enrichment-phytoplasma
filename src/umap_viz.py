from prepare_basic_expression import descriptionParser, get_expression_thresholds_by_time, graphParser, logging, np, parse_LKN, pd, plt, return_temporal_dataframes


def read_mrna_normalized(fpath):
    nst = {}
    cnt = 0
    with open(fpath) as fp:
        for line in fp:
            cnt += 1
            if cnt > 2:
                line = line.strip().split("\t")
                node_name = line[0]
                features = [float(x) for x in line[3:19]]
                assert len(features) == 16
                nst[node_name] = {"normalized_expressions": features}
    return nst


if __name__ == "__main__":

    # comm -12 <(cat list1.txt| sort) <(cat list2.txt|sort)
    # ground_truth_network.csv  mRNA_normalized.csv  mRNA_raw_counts.csv  sRNA_expression.csv
    import umap
    from sklearn import preprocessing

    # datafolder = "../data_drought_clean"
    # mrna_normalized = datafolder+"/results_normalised_filtered_EXP.txt"
    # ground_truth_network = datafolder+"/ground_truth_network.csv"
    # descriptions_file = "../data_drought_clean/gene_bin.txt"

    datafolder = "../data_BN_clean"
    mrna_normalized = datafolder + "/mRNA_normalized.csv"
    ground_truth_network = datafolder + "/ground_truth_network.csv"
    descriptions_file = "../data/vvi_descriptions_ath-sly-stu-sprot_merged.txt"

    ## FIRST OF ALL -> CONSTRUCT mRNA TABLE WITH NORMALIZED INFO
    mrna_normalized_obj = read_mrna_normalized(mrna_normalized)
    mrna_expression_obj = mrna_normalized_obj
    ground_truth_network = graphParser(ground_truth_network)
    description_map = descriptionParser(descriptions_file)
    logging.info("Found {} descriptions.".format(len(description_map)))

    ## to sta prosta parametra za dolociti omrezja!
    measure_threshold = np.arange(0.1, 0.2, 0.02)
    detection_algorithms = ["infomap"]
    ontology_mapping = "../data/ontology.txt"
    ontomap = pd.read_csv(ontology_mapping, sep="\t")
    bins = ontomap.BINCODE.tolist()
    names = ontomap.IDENTIFIER.tolist()
    fmap = dict(zip(names, bins))

    ######################################################################
    ## Step 2: construct co-expression networks between sRNA and mRNA mols
    merged_expression = {**mrna_expression_obj}

    percentile_threshold = 20
    expression_thresholds = get_expression_thresholds_by_time(
        merged_expression, percentile_threshold)

    print("Parsing LKN ..")
    LKN_graph = parse_LKN("../data/LKN.txt")

    ## TAKE 1 - compare community sizes in time.
    ####################################################################
    ## get dataframes for individual time points

    h_t0, p_t0, h_t1, p_t1 = return_temporal_dataframes(
        merged_expression,
        measure="expression",
        exp_lower_bound=expression_thresholds)

    h1 = set(h_t0.columns.values)
    h2 = set(p_t0.columns.values)
    h3 = set(h_t1.columns.values)
    h4 = set(p_t1.columns.values)

    totals = h1.intersection(h2)
    totals = totals.intersection(h3)
    totals = totals.intersection(h4)

    selected_cols = list(totals)

    h_t0 = h_t0[selected_cols]
    p_t0 = p_t0[selected_cols]
    h_t1 = h_t1[selected_cols]
    p_t1 = p_t1[selected_cols]

    exp1 = h_t0.T
    exp2 = p_t0.T
    exp3 = h_t1.T
    exp4 = p_t1.T

    exp_all = pd.concat([exp1, exp2, exp3, exp4], axis=1)
    index_names = exp_all.index.tolist()
    descriptions = []

    for x in index_names:
        if x in fmap:
            descriptions.append(fmap[x].split(".")[0])
        else:
            descriptions.append("unknown")

    colors = [int(x) if "1" in x else 0 for x in descriptions]
    exp_tn = exp_all.T

    x = exp_tn.values  #returns a numpy array
    min_max_scaler = preprocessing.StandardScaler()
    x_scaled = min_max_scaler.fit_transform(x)
    exp_all = pd.DataFrame(x_scaled).T
    reducer = umap.UMAP(n_neighbors=100)
    embedding = reducer.fit_transform(exp_all)
    plt.scatter(embedding[:, 0], embedding[:, 1], c=colors, s=2)
    plt.gca().set_aspect('equal', 'datalim')
    plt.title('Bois Noir', fontsize=16)
    plt.show()
