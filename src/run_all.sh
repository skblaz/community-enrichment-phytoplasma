## community detection + reconstruction + enrichment
python3 enrichment_bois_noir.py

## community selection based on thresholding
python3 merge_data.py --result_folder ../result_community_BN

## output separate aggregates for each time-phenotype
python3 visualize_precomputed_communities.py
