### some helpers
import numpy as np
from tqdm import tqdm
import matplotlib.pyplot as plt
from collections import Counter, defaultdict
from py3plex.core import multinet
from scipy.stats import pearsonr
from itertools import islice, product
import matplotlib.pyplot as plt
import seaborn as sns
import networkx as nx
import multiprocessing as mp
import pandas as pd
from sklearn.metrics import roc_auc_score
from sklearn.metrics import average_precision_score
from scipy.spatial.distance import pdist, squareform
## px imports
from py3plex.core import multinet
#from py3plex.algorithms import hedwig
#from py3plex.algorithms.statistics import enrichment_modules

import os

# Train on CPU (hide GPU) due to memory constraints
os.environ['CUDA_VISIBLE_DEVICES'] = ""
print("importing methods")
import numpy as np

from sklearn.metrics import roc_auc_score
from sklearn.metrics import average_precision_score


def return_pandas_dataframe(attributed_nodes,
                            measure="expression",
                            exp_upper_bound=500):

    datapoints = {}
    for k, v in tqdm(attributed_nodes.items()):
        if measure == "expression":
            exps = v['normalized_expressions']
            if sum(exps) > exp_upper_bound:
                datapoints[k] = exps

        else:
            exps = v['FC']
            datapoints[k] = exps

    df = pd.DataFrame.from_dict(datapoints)
    print(df.shape)
    return df


def return_temporal_dataframes(attributed_nodes,
                               measure="expression",
                               exp_lower_bound=[10, 10, 10, 10]):

    datapoints_j_c = {}
    datapoints_j_p = {}

    datapoints_s_c = {}
    datapoints_s_p = {}

    for k, v in tqdm(attributed_nodes.items()):
        if measure == "expression":
            exps = v['normalized_expressions']
            exps_june_control = exps[0:4]
            exps_june_patho = exps[4:8]
            exps_sep_control = exps[8:12]
            exps_sep_patho = exps[12:16]
            if "miR" in k:
                datapoints_j_c[k] = exps_june_control
                datapoints_j_p[k] = exps_june_patho
                datapoints_s_c[k] = exps_sep_control
                datapoints_s_p[k] = exps_sep_patho
                continue

            if np.mean(exps_june_control) > exp_lower_bound[0]:
                datapoints_j_c[k] = exps_june_control

            if np.mean(exps_june_patho) > exp_lower_bound[1]:
                datapoints_j_p[k] = exps_june_patho

            if np.mean(exps_sep_control) > exp_lower_bound[2]:
                datapoints_s_c[k] = exps_sep_control

            if np.mean(exps_june_control) > exp_lower_bound[3]:
                datapoints_s_p[k] = exps_sep_patho

    df1 = pd.DataFrame.from_dict(datapoints_j_c)
    df2 = pd.DataFrame.from_dict(datapoints_j_p)
    df3 = pd.DataFrame.from_dict(datapoints_s_c)
    df4 = pd.DataFrame.from_dict(datapoints_s_p)

    return (df1, df2, df3, df4)


def correlation_kernel(inpt, threshold=0.9):

    v = inpt[0]
    f = inpt[1]
    ar1 = v[1]['normalized_expressions']
    ar2 = f[1]['normalized_expressions']
    if sum(ar1) > 0 and sum(ar2) > 0:
        corr, _ = pearsonr(ar1, ar2)
        if corr > threshold:
            source_type = "mRNA" if "phasi" not in v[0] else "phasi"
            target_type = "mRNA" if "phasi" not in f[0] else "phasi"
            output_tuple = (v[0], f[0], str(corr), source_type, target_type)
            return output_tuple


def get_correlation_network_parallel(
        attributed_nodes,
        threshold=0.8,
        threads=8,
        batch_size=100,
        outfile="../result_community/initial.edgelist"):

    ar1 = [(k, v) for k, v in attributed_nodes.items()]
    ar2 = [(k, v) for k, v in attributed_nodes.items()]
    pdx = product(ar1, ar2)
    print("Inputs generated..")
    pbar = tqdm(total=int((len(attributed_nodes)**2) / batch_size))
    of = open(outfile, "w+")
    with mp.Pool(processes=threads) as p:
        while 1:
            try:
                top = islice(pdx, batch_size)  # grab the first five elements
                pbar.update()

                results = [
                    "\t".join(x) for x in p.map(correlation_kernel, top)
                    if x != None
                ]
                of.write("\n".join(results))
            except:
                break
    of.close()


def get_correlation_network(attributed_nodes,
                            threshold=0.8,
                            outfile="../result_community/initial.edgelist"):

    Corr_net = multinet.multi_layer_network(directed=False)
    FIL = open(outfile, "w+")
    for k, v in tqdm(attributed_nodes.items()):
        for e, f in attributed_nodes.items():
            ar1 = v['normalized_expressions']
            ar2 = f['normalized_expressions']

            ## take away the noisy ones..
            if sum(ar1) > 0 and sum(ar2) > 0:
                corr, _ = pearsonr(ar1, ar2)
                if corr > threshold:
                    (k, e, str(corr))
                    source_type = "mRNA" if "phasi" not in k else "phasi"
                    target_type = "mRNA" if "phasi" not in e else "phasi"
                    FIL.write("\t".join((k, e, str(corr), source_type,
                                         target_type)) + "\n")

    FIL.close()


def get_node_type(string):
    if "phasi" in string:
        return "phasi"
    elif "Vitvi" in string:
        return "mRNA"
    elif "MIR" in string or "miR" in string:
        return "miRNA"
    else:
        return "unknown"


def get_measure_network_pd(pdf,
                           ctype="x",
                           measure="euclidean",
                           scale_free_test=True,
                           lower_distance_bound = 20): ## vary these expression thresholds if needed.

    current_min_alpha = 0
    current_max_alpha = 10000000

    all_edges = []
    pdf.columns.values
    pdf = pdf.T
    tmp = np.sort(pdf.iloc[:, :], axis=1) ## This is an improvement over the initial naive vector taking -> variance reduction!
    index_names = pdf.index
    pdf_max = pd.Series(tmp[:, 2])
    pdf_min = pd.Series(tmp[:, 1])
    pdf = pd.concat([pdf_max, pdf_min], axis=1)
    pdf.index = index_names

    print("Computing pairwise distances ..")
    distances = np.log(pdist(pdf, measure))
        
    lower_bound_dist = np.percentile(distances, lower_distance_bound)
    distframe = pd.DataFrame(squareform(distances),
                             index=pdf.index,
                             columns=pdf.index)
    all_weights = []
    for column in tqdm(distframe.columns):
        dfx = distframe[[column]]
        dfx_vals = dfx[(dfx > lower_bound_dist)].dropna() ## comment this to get the naive version
        names_link = dfx_vals.index
        evals = dfx_vals.values.tolist()
        for enx, name in enumerate(names_link):
            weight_val = evals[enx][0]
            all_weights.append(weight_val)
            all_edges.append({
                "source": column,
                "target": name,
                "type": "empirical",
                "source_type": get_node_type(column),
                "target_type": get_node_type(name),
                "weight": weight_val
            })

    print("Starting evaluation ..")
    p1 = np.percentile(all_weights, 10) ## This is fine tuned to the phytoplasma dataset -> adapt if needed!
    p2 = np.percentile(all_weights, 20)
    increment = 1
    print(p1, p2, increment)
    threshold = np.arange(p1, p2, increment)
    all_edge_df = pd.DataFrame(all_edges)
    if scale_free_test:
        for thr in threshold:
            print("Testing threshold {}".format(thr))
            subset = all_edge_df.loc[all_edge_df['weight'] < thr] ## lower weight -> all that are closer than this!
            final_edges = subset.to_dict('records')
            print(len(final_edges))
            Corr_net = multinet.multi_layer_network(directed=False)
            try:
                Corr_net.add_edges(final_edges)
            except Exception as es:
                print(es)
                continue
            Corr_net.basic_stats()
            alpha, sigma = Corr_net.test_scale_free()
            if alpha > 2 and alpha < 3:
                current_optimum_network = Corr_net
                print("OPTIMAL ALPHA: {}".format(alpha))
                return current_optimum_network

            elif alpha > current_min_alpha and alpha < current_max_alpha:
                print("Current alpha {}".format(alpha))
                current_optimum_network = Corr_net
                del Corr_net

        return current_optimum_network


def compute_community_intersections(partition_first, partition_second):

    ## node: partition

    partition_storage_first = defaultdict(list)
    for assign, com in partition_first.items():
        partition_storage_first[com].append(assign)

    partition_storage_second = defaultdict(list)
    for assign, com in partition_second.items():
        partition_storage_second[com].append(assign)

    communities_first = list(partition_storage_first.values())
    communities_second = list(partition_storage_second.values())

    result_triplets = []
    for en1, com1 in enumerate(communities_first):
        for en2, com2 in enumerate(communities_second):
            intersection_len = len(set(com1).intersection(set(com2)))
            if intersection_len > 0:
                result_triplets.append((en1, en2, intersection_len))

    result_frame = pd.DataFrame.from_records(result_triplets)
    result_frame.columns = ["c1", "c2", "w"]
    piv = pd.pivot_table(result_frame,
                         values='w',
                         index='c1',
                         columns='c2',
                         fill_value=0)
    return (piv, result_frame)


def jaccard_distance(s1, s2):
    return len(s1.intersection(s2)) / len(s1.union(s2))


def compute_regulation_candidates(ground_truth_network, correlation_network):
    outframe = pd.DataFrame()
    ground_edges = {e for e in ground_truth_network.get_edges()}
    for edge in correlation_network.get_edges(data=True):
        if (edge[0], edge[1]) in ground_edges or (edge[1],
                                                  edge[0]) in ground_edges:
            pass
        else:
            if edge[0][1] == "mRNA" and edge[1][1] == "phasi" or edge[0][
                    1] == "phasi" and edge[1][1] == "mRNA":
                outpoint = {
                    "node_first": edge[0][0],
                    "type_first": edge[0][1],
                    "node_second": edge[1][0],
                    "type_second": edge[1][1],
                    "correlation": edge[2]['correlation_value']
                }
                outframe = outframe.append(outpoint, ignore_index=True)
    return outframe


## first so healthy
def intersect_and_align(merged_expression,
                        partition_first,
                        partition_second,
                        ground_truth_network,
                        top_n=100,
                        target_edge_type="Cleavage",
                        list_of_expressions=None):

    ## healthy
    partition_storage_first = defaultdict(list)
    for assign, com in partition_first.items():
        partition_storage_first[com].append(assign)

    ## infected
    partition_storage_second = defaultdict(list)
    for assign, com in partition_second.items():
        partition_storage_second[com].append(assign)

    jaccard_distances = []
    print("First part -- community similarity")
    for ps1, nds1 in partition_storage_first.items():
        for ps2, nds2 in partition_storage_second.items():
            score = jaccard_distance(set(nds1), set(nds2))
            score2 = 1  #(1/score)*np.exp(1/(len(set(nds1)) + len(set(nds2))))
            jaccard_distances.append((ps1, ps2, score, score2))

    dfx = pd.DataFrame()
    for x in jaccard_distances:
        dfx = dfx.append(
            {
                "com1": x[0],
                "com2": x[1],
                "jaccard": x[2],
                "ejaccard": x[3]
            },
            ignore_index=True)

    print("Second part -- candidate generation")
    alignments = {}
    all_comparisons = []
    for ps1, nds1 in partition_storage_first.items():
        for ps2, nds2 in partition_storage_second.items():
            ints = len(set(nds1).intersection(set(nds2)))
            all_comparisons.append({"com1": ps1, "com2": ps2, "lend": ints})
            if (ps1, ps2) or (ps2, ps1) not in alignments:
                alignments[(ps1, ps2)] = set(nds1).intersection(set(nds2))

    sorted_by_size = sorted(alignments.items(),
                            key=lambda kv: (len(kv[1]), kv[0]),
                            reverse=True)
    candidates = []

    ## get the expressions
    healthy_expressions = list_of_expressions[0]
    infected_expressions = list_of_expressions[1]

    for enx, pair in enumerate(sorted_by_size[0:10]):
        initial_tp, intersection_len = pair
        c1, c2 = initial_tp
        all_nodes = partition_storage_first[c1]
        all_nodes_intersection = alignments[initial_tp]
        disease_exclusive = set(all_nodes) - set(all_nodes_intersection)
        for edge in ground_truth_network.get_edges(data=True):
            n1, n2, dat = edge
            edge_type = dat['type']

            ## first check node presence
            if n1 in disease_exclusive:
                if target_edge_type in edge_type:
                    try:
                        target_node_name = n2[0]
                        fold_change_second = np.log(
                            np.mean(infected_expressions[[target_node_name
                                                          ]].as_matrix()) /
                            np.mean(healthy_expressions[[target_node_name
                                                         ]].as_matrix()))
                        fold_change_first = np.log(
                            np.mean(infected_expressions[[n1[0]
                                                          ]].as_matrix()) /
                            np.mean(healthy_expressions[[n1[0]]].as_matrix()))
                        out_tuple = (n1[0], n1[1], edge_type, n2[0], n2[1],
                                     str(fold_change_first),
                                     str(fold_change_second))
                        candidates.append(out_tuple)
                    except:
                        pass

            # ## first check node presence
            # if n2 in disease_exclusive:
            #     if target_edge_type in edge_type:
            #         try:
            #             target_node_name = n1[0]
            #             fold_change = np.log(np.mean(np.array(infected_expressions[[target_node_name]].values))/np.mean(np.array(healthy_expressions[[target_node_name]].values)))
            #             out_tuple = (n2[0],n2[1],edge_type,n1[0],n1[1],str(fold_change), str(enx))
            #             candidates.append(out_tuple)
            #         except:
            #             pass

    return (candidates, all_comparisons, dfx)


def summarise_communities(communities, name="test"):

    sizes = []
    partition_storage_second = defaultdict(list)
    for assign, com in communities.items():
        partition_storage_second[com].append(assign)

    for x, v in partition_storage_second.items():
        sizes.append(len(v))

    print(name + " " + "average size: " + str(np.mean(sizes)) + " max size:" +
          str(np.max(sizes)))


def export_communities_to_obj(community_list=None, names=None):

    out_obj = pd.DataFrame()
    for enx, community in enumerate(community_list):
        community_time_state = names[enx]
        communities_obj = defaultdict(list)
        # ('Vitvi12g00066', 'mRNA') 1 - this is some form of target community.
        for k, v in community.items():
            communities_obj[v].append(k[0])

        for k, v in communities_obj.items():
            members = ",".join(v)
            out_obj = out_obj.append(
                {
                    "id": str(k) + "_" + community_time_state,
                    "members": members
                },
                ignore_index=True)

    return out_obj


def generate_jaccard_candidates(dfx,
                                communities,
                                size_threshold=3,
                                max_size=1000):

    ## infected
    dx3 = dfx.groupby(['com2'])['jaccard'].max().reset_index()
    dx3.columns = ['com2', 'max_jaccard']
    dx3 = dx3.sort_values(by=['max_jaccard'], ascending=True)

    partition_storage_first = defaultdict(list)
    for assign, com in communities.items():
        partition_storage_first[com].append(assign)

    den = dict(zip(dx3.com2, dx3.max_jaccard))
    candidate_communities = {}

    for k, v in den.items():
        candidate_community = partition_storage_first[int(k)]
        if len(set(candidate_community)) > size_threshold and len(
                set(candidate_community)) < max_size:
            candidate_communities[k] = {
                "community": [x[0] for x in candidate_community],
                "score": v
            }

    return candidate_communities


def convert_to_gaf(infile, tabu_term="35."):
    ## input mappings must be in form of GAF files!
    ## UniProtKB	A0A075B6R9	IGKV2D-24		GO:0002377	GO_REF:0000033	IBA	PANTHER:PTN000587099	P	Immunoglobulin kappa variable 2D-24 (non-functional)	A0A075B6R9_HUMAN|IGKV2D-24	protein	taxon:9606	20150528	GO_Central

    out_gaf_mappings = []
    invalid_ents = 0
    annotation_pairs = []
    with open(infile) as inf:
        for line in inf:
            try:
                parts = line.strip().split("\t")
                if tabu_term not in parts[0]:
                    annotation_pairs.append((parts[0], parts[1]))
                    gaf_mapping = "\t".join(
                        ["#", parts[2], "#", "#", parts[0]])
                    out_gaf_mappings.append(gaf_mapping)
            except:
                invalid_ents += 1

    print("{} invalid mappings.".format(invalid_ents))
    return (out_gaf_mappings, dict(annotation_pairs))


def fet_enrichment(partition,
                   gaf_file="../BK/gmm.gaf",
                   annotation_file=None,
                   name="test",
                   alternative="two-sided",
                   multitest_method="bonferroni",
                   pvalue=0.01):

    ## uniprot : node pairs are used as input! Generic example TBA
    community_object = defaultdict(set)
    for node, community in partition.items():
        community_object[community].add(node[0])  ## add only node names!

    ## p<0.05 and fdr_bh correction for GO function -- this can take some time!
    enrichment_table = enrichment_modules.fet_enrichment_terms(
        community_object,
        gaf_file,
        alternative=alternative,
        intra_community=False,
        pvalue=pvalue,
        multitest_method=multitest_method)

    descs = []
    terms = enrichment_table.term

    for term in terms:
        try:
            descs.append(annotation_file[term])
        except:
            descs.append("no annotation")
    enrichment_table['id'] = enrichment_table['observation'].astype(
        str) + "_" + name
    enrichment_table['descriptions'] = descs
    return enrichment_table


def CBSSD_enrichment(
        partitions,
        gaf_file,
        dataset_name="../semantic_datasets/example_partition_inputs.n3",
        layer_type="dummy",
        obo_ontology="../data/ontology.obo",
        names=None,
        output_rules="../result_community/hedwig_run1.json"):

    merged_partitions = defaultdict(list)
    ## merge partitions and learn
    for en, x in enumerate(partitions):
        name = names[en]
        for k, v in x.items():
            merged_partitions[k] = str(v) + "_" + name

    ## convert examples to RDF mappings and check the validity, use gzipped gaf files..
    rdf_partitions = hedwig.convert_mapping_to_rdf(
        merged_partitions,
        annotation_mapping_file=gaf_file,
        layer_type=None,
        go_identifier=None,
        prepend_string="GMM:")

    rdf_partitions.serialize(destination=dataset_name, format="n3")

    ## convert obo file to n3
    hedwig.obo2n3(obo_ontology, "../BK/bk.n3", gaf_file)

    ## some default input parameters
    hedwig_input_parameters = {
        "bk_dir": "../BK",
        "data": "../semantic_datasets/example_partition_inputs.n3",
        "format": "n3",
        "output": output_rules,
        "covered": None,
        "mode": "subgroups",
        "target": None,
        "score": "lift",
        "negations": True,
        "alpha": 0.05,
        "latex_report": False,
        "adjust": "fwer",
        "FDR": 0.05,
        "leaves": True,
        "learner": "heuristic",
        "optimalsubclass": False,
        "uris": False,
        "beam": 80,
        "support": 0.01,
        "depth": 20,
        "nocache": True,
        "verbose": False,
        "adjust": "fdr"
    }

    ## initiate the learning part (TODO: export this as table of some sort)
    print("Starting learning")
    hedwig.run(hedwig_input_parameters)


def parse_LKN(lknfile):
    LKN_net = multinet.multi_layer_network(directed=True)
    edges = []
    with open(lknfile) as lkf:
        for line in lkf:
            line = line.strip()
            name_first, name_second, intype, _ = line.strip().split()
            edges.append({
                "source": name_first,
                "target": name_second,
                "type": intype + "_LKN",
                "source_type": get_node_type(name_first),
                "target_type": get_node_type(name_second)
            })
    LKN_net.add_edges(edges)
    LKN_net.basic_stats()
    return LKN_net


def generate_feature_matrix(network, features):
    unique_types = {}
    node_feature_vectors = []
    cntr = 1
    for node in network.get_nodes(data=True):
        nname = node[0][0]
        feature_vec = np.zeros(17)
        if node[0][1] in unique_types.keys():
            type_feature = unique_types[node[0][1]]
        else:
            unique_types[node[0][1]] = cntr
            type_feature = cntr
            cntr += 1
        if nname in features:
            feature_vec1 = features.get(nname)['normalized_expressions']
            feature_vec[1:] = feature_vec1
        else:
            pass
        feature_vec[0] = type_feature
        node_feature_vectors.append(feature_vec)
    feature_matrix = np.matrix(node_feature_vectors)
    adj = network.to_sparse_matrix(return_only=True)
    print(feature_matrix.shape, adj.shape)
    return adj, feature_matrix


def get_roc_score(edges_pos, edges_neg, score_matrix, adj_sparse):
    # Store positive edge predictions, actual values
    preds_pos = []
    pos = []
    for edge in edges_pos:
        preds_pos.append(score_matrix[edge[0], edge[1]])  # predicted score
        pos.append(adj_sparse[edge[0],
                              edge[1]])  # actual value (1 for positive)

    # Store negative edge predictions, actual values
    preds_neg = []
    neg = []
    for edge in edges_neg:
        preds_neg.append(score_matrix[edge[0], edge[1]])  # predicted score
        neg.append(adj_sparse[edge[0],
                              edge[1]])  # actual value (0 for negative)

    # Calculate scores
    preds_all = np.hstack([preds_pos, preds_neg])
    labels_all = np.hstack([np.ones(len(preds_pos)), np.zeros(len(preds_neg))])
    roc_score = roc_auc_score(labels_all, preds_all)
    ap_score = average_precision_score(labels_all, preds_all)
    return roc_score, ap_score


def benchmark_baselines(X):

    adj_train, train_edges, train_edges_false, val_edges, val_edges_false, test_edges, test_edges_false = mask_test_edges(
        X, test_frac=.3, val_frac=.1)
    g_train = nx.from_scipy_sparse_matrix(
        adj_train)  # new graph object with only non-hidden edges
    # Inspect train/test split
    print("Total nodes:", X.shape[0])
    print("Total edges:", int(X.nnz / 2))
    print("Training edges (positive):", len(train_edges))
    print("Training edges (negative):", len(train_edges_false))
    print("Validation edges (positive):", len(val_edges))
    print("Validation edges (negative):", len(val_edges_false))
    print("Test edges (positive):", len(test_edges))
    print("Test edges (negative):", len(test_edges_false))

    # Compute Adamic-Adar indexes from g_train
    aa_matrix = np.zeros(X.shape)
    for u, v, p in nx.adamic_adar_index(
            g_train):  # (u, v) = node indices, p = Adamic-Adar index
        aa_matrix[u][v] = p
        aa_matrix[v][u] = p  # make sure it's symmetric

    # Normalize array
    aa_matrix = aa_matrix / aa_matrix.max()

    # Calculate ROC AUC and Average Precision
    aa_roc, aa_ap = get_roc_score(test_edges, test_edges_false, aa_matrix, X)

    print('Adamic-Adar Test ROC score: ', str(aa_roc))
    print('Adamic-Adar Test AP score: ', str(aa_ap))

    # Compute Jaccard Coefficients from g_train
    jc_matrix = np.zeros(X.shape)
    for u, v, p in nx.jaccard_coefficient(
            g_train):  # (u, v) = node indices, p = Jaccard coefficient
        jc_matrix[u][v] = p
        jc_matrix[v][u] = p  # make sure it's symmetric

    # Normalize array
    jc_matrix = jc_matrix / jc_matrix.max()

    # Calculate ROC AUC and Average Precision
    jc_roc, jc_ap = get_roc_score(test_edges, test_edges_false, jc_matrix, X)

    print('Jaccard Coefficient Test ROC score: ', str(jc_roc))
    print('Jaccard Coefficient Test AP score: ', str(jc_ap))

    # Calculate, store Adamic-Index scores in array
    pa_matrix = np.zeros(X.shape)
    for u, v, p in nx.preferential_attachment(
            g_train):  # (u, v) = node indices, p = Jaccard coefficient
        pa_matrix[u][v] = p
        pa_matrix[v][u] = p  # make sure it's symmetric

    # Normalize array
    pa_matrix = pa_matrix / pa_matrix.max()

    # Calculate ROC AUC and Average Precision
    pa_roc, pa_ap = get_roc_score(test_edges, test_edges_false, pa_matrix, X)

    print('Preferential Attachment Test ROC score: ', str(pa_roc))
    print('Preferential Attachment Test AP score: ', str(pa_ap))


def preferential_attachment_all(LKN):

    cands = []
    try:
        cnet = LKN.core_network
    except:
        cnet = LKN
    print(nx.info(cnet))
    simple_graph = nx.Graph()
    for edge in cnet.edges(data=True):
        n1 = edge[0]
        n2 = edge[1]
        simple_graph.add_node(n1)
        simple_graph.add_node(n2)
        simple_graph.add_edge(n1, n2)
        cands.append((n1, n2))
    print(len(cands))
    print(nx.info(simple_graph))
    candidate_interactions = []
    for u, v, p in nx.preferential_attachment(simple_graph, cands):
        out_str = "\t".join(str(x) for x in [u[0], u[1], v[0], v[1], p])
        candidate_interactions.append(out_str)
    return candidate_interactions


def preferential_attachment_preds(LKN):

    cands = []
    cnet = LKN.core_network
    simple_graph = nx.Graph()
    for edge in cnet.edges(data=True):
        n1 = edge[0]
        n2 = edge[1]
        simple_graph.add_node(n1)
        simple_graph.add_node(n2)
        simple_graph.add_edge(n1, n2)

        if n1[1] == "miRNA" or n2[1] == "miRNA":
            cands.append((n1, n2))
    print(len(cands))
    candidate_interactions = []
    for u, v, p in nx.preferential_attachment(simple_graph, cands):
        out_str = "\t".join(str(x) for x in [u[0], u[1], v[0], v[1], p])
        candidate_interactions.append(out_str)
    return candidate_interactions


def dissipation_coefficient(community, partition):

    distinct_counter = defaultdict(set)
    total_size = len(community)
    at_least_one = False
    for member in community:
        for com, mems in partition.items():
            if member in mems:
                at_least_one = True
                distinct_counter[com].add(member)
    if at_least_one:
        dissipation_index = 1
        dissipation_indi = []
        for community, member in distinct_counter.items():
            dissipation_index = len(member) / total_size
            dissipation_indi.append(dissipation_index)
        try:
            real_index = np.max(dissipation_indi) / len(dissipation_indi)
            return -np.log(real_index)
        except:
            return 0
    else:
        return 0


def community_dissipation_vertical(j_h,
                                   j_p,
                                   s_h,
                                   s_p,
                                   names=["J_H", "J_I", "S_H", "S_I"],
                                   mapping=None):
    ## first do september
    communities_june_healthy = defaultdict(set)
    communities_september_healthy = defaultdict(set)

    for node, community in j_h.items():
        communities_june_healthy[community].add(node)

    for node, community in s_h.items():
        communities_september_healthy[community].add(node)

    dissipations_healthy = []
    for community, members in communities_june_healthy.items():
        dissipation_index = dissipation_coefficient(
            members, communities_september_healthy)
        output_string_tuple = {
            "members": ";".join([x[0] for x in members]),
            "size": len(members),
            "dissipation": dissipation_index,
            "state": "healthy",
            "direction": "t0 -> t1",
            "name": str(community) + "_" + names[0]
        }
        dissipations_healthy.append(output_string_tuple)

    communities_june_infected = defaultdict(set)
    communities_september_infected = defaultdict(set)

    for node, community in j_p.items():
        communities_june_infected[community].add(node)

    for node, community in s_p.items():
        communities_september_infected[community].add(node)

    dissipations_infected = []
    for community, members in communities_june_infected.items():
        dissipation_index = dissipation_coefficient(
            members, communities_september_infected)
        output_string_tuple = {
            "members": ";".join([x[0] for x in members]),
            "size": len(members),
            "dissipation": dissipation_index,
            "state": "infected",
            "direction": "t0 -> t1",
            "name": str(community) + "_" + names[1]
        }
        dissipations_infected.append(output_string_tuple)

    for community, members in communities_september_healthy.items():
        dissipation_index = dissipation_coefficient(members,
                                                    communities_june_healthy)
        output_string_tuple = {
            "members": ";".join([x[0] for x in members]),
            "size": len(members),
            "dissipation": dissipation_index,
            "state": "healthy",
            "direction": "t1 -> t0",
            "name": str(community) + "_" + names[2]
        }
        dissipations_infected.append(output_string_tuple)

    for community, members in communities_september_infected.items():
        dissipation_index = dissipation_coefficient(members,
                                                    communities_june_infected)
        output_string_tuple = {
            "members": ";".join([x[0] for x in members]),
            "size": len(members),
            "dissipation": dissipation_index,
            "state": "infected",
            "direction": "t1 -> t0",
            "name": str(community) + "_" + names[3]
        }
        dissipations_infected.append(output_string_tuple)

    all_dis = dissipations_healthy + dissipations_infected
    dissipations_all = pd.DataFrame(all_dis)
    dissipations_all = add_annotations_enrichment(dissipations_all,
                                                  mapping,
                                                  limit_num=50,
                                                  delimiter=";")
    return dissipations_all


def community_dissipation(j_h,
                          j_p,
                          s_h,
                          s_p,
                          names=["J_H", "J_I", "S_H", "S_I"],
                          mapping=None):

    ## first do september
    communities_september_healthy = defaultdict(set)
    communities_september_infected = defaultdict(set)

    for node, community in s_h.items():
        communities_september_healthy[community].add(node)

    for node, community in s_p.items():
        communities_september_infected[community].add(node)

    dissipations_september = []
    for community, members in communities_september_healthy.items():
        dissipation_index = dissipation_coefficient(
            members, communities_september_infected)
        output_string_tuple = {
            "members": ";".join([x[0] for x in members]),
            "size": len(members),
            "dissipation": dissipation_index,
            "time": "autumn",
            "direction": "healthy -> infected",
            "name": str(community) + "_" + names[2]
        }
        dissipations_september.append(output_string_tuple)

    communities_june_healthy = defaultdict(set)
    communities_june_infected = defaultdict(set)

    for node, community in j_h.items():
        communities_june_healthy[community].add(node)

    for node, community in j_p.items():
        communities_june_infected[community].add(node)

    dissipations_june = []
    for community, members in communities_june_healthy.items():
        dissipation_index = dissipation_coefficient(members,
                                                    communities_june_infected)
        output_string_tuple = {
            "members": ";".join([x[0] for x in members]),
            "size": len(members),
            "dissipation": dissipation_index,
            "time": "spring",
            "direction": "healthy -> infected",
            "name": str(community) + "_" + names[0]
        }
        dissipations_june.append(output_string_tuple)

    dissipations_june_inverse = []
    for community, members in communities_june_infected.items():
        dissipation_index = dissipation_coefficient(members,
                                                    communities_june_healthy)
        output_string_tuple = {
            "members": ";".join([x[0] for x in members]),
            "size": len(members),
            "dissipation": dissipation_index,
            "time": "spring",
            "direction": "infected -> healthy",
            "name": str(community) + "_" + names[1]
        }
        dissipations_june_inverse.append(output_string_tuple)

    dissipations_september_inverse = []
    for community, members in communities_september_infected.items():
        dissipation_index = dissipation_coefficient(
            members, communities_september_healthy)
        output_string_tuple = {
            "members": ";".join([x[0] for x in members]),
            "size": len(members),
            "dissipation": dissipation_index,
            "time": "autumn",
            "direction": "infected -> healthy",
            "name": str(community) + "_" + names[3]
        }
        dissipations_september_inverse.append(output_string_tuple)

    all_dis = dissipations_june + dissipations_september + dissipations_september_inverse + dissipations_june_inverse

    dissipations_all = pd.DataFrame(all_dis)
    dissipations_all = add_annotations_enrichment(dissipations_all,
                                                  mapping,
                                                  limit_num=50,
                                                  delimiter=";")

    return dissipations_all


def descriptionParser(dfile):

    idents = {}
    with open(dfile) as df:
        for line in df:
            line = line.strip().split()
            identifier = line[0]
            name = " ".join(line[1:])
            if "." in identifier:
                identifier = identifier.split(".")[0]
            idents[identifier] = name
    return idents


def add_annotations_enrichment(dfile, mapping, limit_num=50, delimiter=","):

    new = []
    for en, row in dfile.iterrows():
        rows = row.keys()
        members = row.members.split(delimiter)
        descs = []
        for member in members:
            if member in mapping:
                descs.append(mapping[member])
            else:
                descs.append("no description")
        dk = dict(Counter(descs))
        counts = [dk[x] for x in descs]
        for j in range(len(members)):
            new_row = {}
            for k in rows:
                if k != "members":
                    new_row[k] = row[k]
            new_row['RNA ID'] = members[j]
            new_row['RNA description'] = descs[j]
            new_row['Desc count'] = counts[j]
            new.append(new_row)
    newfile = pd.DataFrame(new)
    return newfile


def compare_pairwise_expressions(graph_first,
                                 merged_expression,
                                 comparison_name="horizontal",
                                 mapping=None):

    candidate_edges = []
    cntx = 0
    for edge in graph_first.get_edges(data=True):
        if edge[0][1] == "mRNA" and edge[1][1] == "miRNA" or edge[1][
                1] == "mRNA" and edge[0][1] == "miRNA":

            int_type = edge[2]['type']
            mRNA_name = None

            ## get description here!
            if edge[0][1] == "mRNA":
                pass
            else:
                first = edge[1]
                second = edge[0]
                edge = [None, None]
                edge[0] = first
                edge[1] = second

            mRNA_name = edge[0][0]
            try:
                expressions_mRNA = np.exp(
                    merged_expression[edge[0][0]]['normalized_expressions'])
                exps_june_control_mRNA = np.array(expressions_mRNA[0:4])
                exps_june_patho_mRNA = np.array(expressions_mRNA[4:8])
                exps_sep_control_mRNA = np.array(expressions_mRNA[8:12])
                exps_sep_patho_mRNA = np.array(expressions_mRNA[12:16])

                expressions_miRNA = merged_expression[
                    edge[1][0]]['normalized_expressions']
                exps_june_control_miRNA = np.array(expressions_miRNA[0:4])
                exps_june_patho_miRNA = np.array(expressions_miRNA[4:8])
                exps_sep_control_miRNA = np.array(expressions_miRNA[8:12])

                exps_sep_patho_miRNA = np.array([
                    np.max(np.array(expressions_miRNA[12:16])),
                    np.min(np.array(expressions_miRNA[12:16]))
                ])
                exps_sep_control_miRNA = np.array([
                    np.max(np.array(expressions_miRNA[8:12])),
                    np.min(np.array(expressions_miRNA[8:12]))
                ])
                exps_june_patho_miRNA = np.array([
                    np.max(np.array(expressions_miRNA[4:8])),
                    np.min(np.array(expressions_miRNA[4:8]))
                ])
                exps_june_control_miRNA = np.array([
                    np.max(np.array(expressions_miRNA[0:4])),
                    np.min(np.array(expressions_miRNA[0:4]))
                ])

                # exp_dist_spring_control= np.linalg.norm(exps_june_control_mRNA-exps_june_control_miRNA)
                # exp_dist_spring_patho= np.linalg.norm(exps_june_patho_mRNA-exps_june_patho_miRNA)
                # exp_dist_sep_control= np.linalg.norm(exps_sep_control_mRNA-exps_sep_control_miRNA)
                # exp_dist_sep_patho= np.linalg.norm(exps_sep_patho_mRNA-exps_sep_patho_miRNA)

                # horizontal_june = np.linalg.norm(exp_dist_spring_patho - exp_dist_spring_control)
                # horizontal_sep = np.linalg.norm(exp_dist_sep_patho - exp_dist_sep_control)
                # vertical_healthy = np.linalg.norm(exp_dist_spring_control - exp_dist_sep_control)
                # vertical_patho = np.linalg.norm(exp_dist_spring_patho - exp_dist_sep_patho)

                vertical_patho = np.linalg.norm(exps_sep_patho -
                                                exps_june_patho)
                out_obj = {
                    'mRNA': mRNA_name,
                    'miRNA': edge[1][0],
                    "miRNA activity": vertical_patho,
                    "interaction tag": int_type
                }

                #out_obj = {'mRNA':mRNA_name,'miRNA':edge[1][0],"horizontal - June (h vs. p)":horizontal_june,"horizontal - Sep (h vs. p)":horizontal_sep,"vertical_healthy":vertical_healthy,"vertical_patho":vertical_patho,"interaction tag":int_type}
                candidate_edges.append(out_obj)

            except Exception:

                cntx += 1

    print("No expression info for {} genes.".format(cntx))
    fframe = pd.DataFrame(candidate_edges)
    print(fframe)
    return fframe


def visualize_umap(expressions):

    entries = []
    names = []

    for k, v in expressions.items():
        names.append(get_node_type(k))
        entries.append(v['normalized_expressions'])
    vals = pd.DataFrame.from_records(entries)
    import umap
    print("Starting projections")
    dfx = pd.DataFrame()
    embedding = umap.UMAP(n_neighbors=10).fit_transform(vals)
    dfx['mol'] = names
    dfx['d1'] = embedding[:, 0]
    dfx['d2'] = embedding[:, 1]
    sizes = [10 if x == "miRNA" else 2 for x in names]
    sns.scatterplot(dfx.d1, dfx.d2, hue=dfx['mol'], s=sizes)
    plt.show()


def get_expression_thresholds_by_time(expression_object,
                                      perc,
                                      col_format="BN"):

    if col_format == "BN":

        ## Goal: adapt expression thresholds for each time point!
        expressions_spring_healthy = []
        expressions_spring_infected = []
        expressions_autumn_healthy = []
        expressions_autumn_infected = []

        for k, v in expression_object.items():
            exps = v['normalized_expressions']

            exps_june_control = exps[0:4]
            exps_june_patho = exps[4:8]
            exps_sep_control = exps[8:12]
            exps_sep_patho = exps[12:16]

            expressions_spring_healthy.append(exps_june_control)
            expressions_spring_infected.append(exps_june_patho)
            expressions_autumn_healthy.append(exps_sep_control)
            expressions_autumn_infected.append(exps_sep_patho)

        ## flatten
        e_J_H = np.matrix(expressions_spring_healthy).flatten()
        e_J_I = np.matrix(expressions_spring_infected).flatten()
        e_S_H = np.matrix(expressions_autumn_healthy).flatten()
        e_S_I = np.matrix(expressions_autumn_infected).flatten()

        # mean_JH = np.mean(e_J_H)
        # mean_JI = np.mean(e_J_I)
        # mean_SH = np.mean(e_S_H)
        # mean_SI = np.mean(e_S_I)

        # std_JH = np.std(e_J_H)
        # std_JI = np.std(e_J_I)
        # std_SH = np.std(e_S_H)
        # std_SI = np.std(e_S_I)

        perc_JH = np.percentile(e_J_H, perc)
        perc_JI = np.percentile(e_J_I, perc)
        perc_SH = np.percentile(e_S_H, perc)
        perc_SI = np.percentile(e_S_I, perc)

        print("Thresholds used: {} {} {} {}".format(perc_JH, perc_JI, perc_SH,
                                                    perc_SI))
        return (perc_JH, perc_JI, perc_SH, perc_SI)

    elif col_format == "DROUGHT_CABERNET":

        ## Goal: adapt expression thresholds for each time point!
        expressions_t0_W = []
        expressions_t0_S = []
        expressions_t1_W = []
        expressions_t1_S = []

        for k, v in expression_object.items():
            exps = v['normalized_expressions']
            exps_t0_W = exps[4:8]
            exps_t0_S = exps[:4]
            exps_t1_S = exps[16:20]
            exps_t1_W = exps[20:24]

            expressions_t0_W.append(exps_t0_W)
            expressions_t0_S.append(exps_t0_S)
            expressions_t1_W.append(exps_t1_W)
            expressions_t1_S.append(exps_t1_S)

        ## flatten
        e_t0_W = np.matrix(expressions_t0_W).flatten()
        e_t0_S = np.matrix(expressions_t0_S).flatten()
        e_t1_W = np.matrix(expressions_t1_W).flatten()
        e_t1_S = np.matrix(expressions_t1_S).flatten()

        perc_JH = np.percentile(e_t0_W, perc)
        perc_JI = np.percentile(e_t0_S, perc)
        perc_SH = np.percentile(e_t1_W, perc)
        perc_SI = np.percentile(e_t1_S, perc)

        print("Thresholds used: {} {} {} {}".format(perc_JH, perc_JI, perc_SH,
                                                    perc_SI))
        return (perc_JH, perc_JI, perc_SH, perc_SI)
