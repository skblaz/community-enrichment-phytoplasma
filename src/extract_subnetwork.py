## extract c2:
import networkx as nx
import numpy as np
from scipy.spatial.distance import pdist, squareform
import pandas as pd
import matplotlib.pyplot as plt

print("Reading")
#graph = nx.read_gpickle("../result_community_BN/nnet.gpickle").to_undirected()

hjune = pd.read_csv("../result_community_BN/hjune.tsv", sep="\t")

subnet = [
    'Vitvi00g01098', 'Vitvi18g02758', 'Vitvi06g00956', 'Vitvi18g00445',
    'Vitvi18g02012', 'Vitvi08g01043', 'Vitvi18g02758', 'Vitvi06g00956',
    'Vitvi18g00445', 'Vitvi18g02012', 'Vitvi00g01098', 'Vitvi08g01043',
    'Vitvi18g02758', 'Vitvi06g00956', 'Vitvi18g00445', 'Vitvi18g02012'
]

subnet2 = [
    'Vitvi07g03081',
    'Vitvi01g01482',
    'Vitvi08g02107',
    'Vitvi07g02188',
    'Vitvi10g00740',
    #    'Vitvi11g00097',
    'Vitvi03g01524',
    'Vitvi05g01860',
    'Vitvi16g00810'
]

overall_subset = subnet + subnet2
pdf = hjune[overall_subset]
pdf = pdf.T
tmp = np.sort(pdf.iloc[:, :], axis=1)
index_names = pdf.index
pdf_max = pd.Series(tmp[:, 2])
pdf_min = pd.Series(tmp[:, 1])
pdf = pd.concat([pdf_max, pdf_min], axis=1)
pdf.index = index_names
distances = np.log(pdist(pdf, 'euclidean'))
distances[distances == -np.inf] = 0
distframe = pd.DataFrame(squareform(distances),
                         index=pdf.index,
                         columns=pdf.index)
graph = nx.Graph()

# print(np.mean(distframe.values))
for a in distframe.columns:
    for b in distframe.index:
        if a != b:
            distance = distframe.loc[a, b]
            try:
                mdist = np.mean(distance).values[0]
            except:
                mdist = np.mean(distance)
            if mdist < 4.30:  ## the paper threshold
                graph.add_edge(a, b, weight=mdist)

pos = nx.spring_layout(graph)
colors = []
for n in graph.nodes():
    if n in subnet:
        colors.append("red")
    else:
        colors.append("green")
nx.draw_networkx_nodes(graph, pos=pos, node_color=colors)
nx.draw_networkx_edges(graph, pos=pos)
nx.draw_networkx_labels(graph, pos=pos)
plt.show()
# nx.draw(graph, with_labels = True)
# plt.show()
# for e in graph.edges():
#     e0 = e[0]
#     e1 = e[1]
#     if e0 != e1:
#         print("\t".join([e0, e1]))
