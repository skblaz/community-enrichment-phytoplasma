from py3plex.core import multinet
from py3plex.visualization.multilayer import np
import pandas as pd
import tqdm
import os
os.system("rm -rvf ../communities_BN_for_cytoscape/*")
os.system("rm -rvf ../separate_BN_community_aggregates/*")
os.system("rm -rvf ../ranked_miRNA_links/*")

#import SCD
from collections import defaultdict

import networkx as nx

# community_files = ["network_sep_p.gpickle",
#                    "network_sep_h.gpickle",
#                    "network_june_h.gpickle",
#                    "network_june_p.gpickle","JOINT"]

raw_community_files = [
    "network_sep_p.gpickle", "network_sep_h.gpickle", "network_june_h.gpickle",
    "network_june_p.gpickle"
]

community_files = ["JOINT"]
# community_files = ["network_sep_p.gpickle",
#                     "network_sep_h.gpickle",
#                     "network_june_h.gpickle",
#                     "network_june_p.gpickle"]

cfx = "../result_community_BN/ALL_infomap_COMMUNITIES.tsv"


def read_cfile(comfile):

    dfx = defaultdict(list)
    with open(comfile) as cfm:
        for line in cfm:
            try:
                id2, cid, nodes = line.strip().split("\t")
                dfx[cid] = list()
                for node in nodes.split(","):
                    dfx[cid].append(node)
            except:
                pass
    return dfx


def add_enrichment_annotation(graph, gmap):

    dfx2 = []
    for e in graph.edges(data=True):
        if "miRNA" in e[0][1] or "miRNA" in e[1][1]:
            i1 = e[0][0]
            i2 = e[1][0]
            w = e[2]['weight']
            row = (i1, i2, w)
            dfx2.append(row)

    dfx2 = pd.DataFrame(dfx2)
    new_df = []

    dfx2.columns = ['Interactor A', 'Interactor B', 'weight']

    for idx, row in dfx2.iterrows():
        i1 = row['Interactor A']
        i2 = row['Interactor B']
        w = row['weight']

        if i1 in gmap:
            description_first = gmap[i1][0]
            description_first2 = gmap[i1][1]

        else:
            description_first = "na"
            description_first2 = "na"

        if i2 in gmap:
            description_second = gmap[i2][0]
            description_second2 = gmap[i2][1]

        else:
            description_second = "na"
            description_second2 = "na"

        row = [
            i1, i2, w, description_first, description_first2,
            description_second, description_second2
        ]
        new_df.append(row)

    new_df2 = pd.DataFrame(new_df)
    new_df2.columns = [
        'Interactor A', 'Interactor B', 'weight', 'Description A', 'Bin A',
        'Description B', 'Bin B'
    ]
    new_df2 = new_df2.sort_values(by=['weight'], ascending=False)
    new_df2 = new_df2.drop_duplicates(
        subset=['Interactor A', 'Interactor B', 'weight'])
    return new_df2


ddict = read_cfile(cfx)
ontofile = pd.read_csv('../data/ontology.txt', sep="\t")
ontofile.columns = ['bin', 'description', 'gene', 'gene2']
gmap = {}

for idx, row in ontofile.iterrows():
    gid = row['gene']
    description = row['description']
    rb = row['bin']
    gmap[gid] = [description, rb]

for gfile in community_files:

    if not gfile == "JOINT":
        print("Extracting {}".format(gfile))
        graph = nx.read_gpickle(
            "../result_community_BN/{}".format(gfile)).to_undirected()
        print(nx.info(graph))
    else:
        graphs = [
            nx.read_gpickle("../result_community_BN/{}".format(x))
            for x in raw_community_files
        ]
        U = nx.Graph()
        for gx in graphs:
            U.add_edges_from(gx.edges(data=True))
        graph = U

    isolated = list(nx.isolates(graph))
    graph.remove_nodes_from(list(nx.isolates(graph)))

    miRNA_graph = defaultdict(list)
    for e in graph.edges(data=True):
        if "miRNA" in e[0][1]:
            node_mirna = e[0][0]
            node_mRNA = e[1][0]
        elif "miRNA" in e[1][1]:
            node_mirna = e[1][0]
            node_mRNA = e[0][0]
        w = e[2]['weight']
        tuplet = (node_mirna, w)
        miRNA_graph[node_mRNA].append(tuplet)

    enrichment_df = add_enrichment_annotation(graph, gmap)
    fnx = gfile.replace(".gpickle", "")
    enrichment_df.to_csv("../ranked_miRNA_links_BN/{}.tsv".format(fnx),
                         sep="\t")
    original_nodes = set(x[0] for x in graph.nodes())

    G = multinet.multi_layer_network()
    G = nx.Graph()

    for edge in graph.edges(data=True):
        ## add a single edge with type
        weight = edge[2]['weight']
        if weight != -np.inf:
            G.add_node(edge[0][0], type=edge[0][1])
            G.add_node(edge[1][0], type=edge[1][1])
            G.add_edge(edge[0][0], edge[1][0], weight=weight)

    graph = G.copy()
    full_graph = G.copy()
    nodes2 = set(x for x in G.nodes())
    intersection = nodes2.intersection(original_nodes)
    print(len(intersection), len(nodes2))
    for k, v in tqdm.tqdm(ddict.items()):
        cid = k
        if "june_p" in gfile and "J_I" in k:
            pass
        elif "june_h" in gfile and "J_H" in k:
            pass
        elif "sep_h" in gfile and "S_H" in k:
            pass
        elif "sep_p" in gfile and "S_I" in k:
            pass
        else:
            if "JOINT" in gfile:
                pass
            else:
                continue
        dataset_rows = []
        if len(v) >= 2 and len(v) < 300:
            subgraph = graph.subgraph(v)
            all_links = []
            if len(subgraph.edges()) < 2:
                continue

            # miRNA_graph
            to_add_links = []
            mirna_weights = 0
            mirnas = set([x for x in full_graph.nodes() if "miR" in x])
            for link in subgraph.edges(data=True):
                n1, n2, data = link
                weight = data['weight']
                if "miR" in n1 or "miR" in n2:
                    mirna_weights += 1
                to_add_links.append((n1, n2, 1))
                neighbors_first = full_graph[n1]
                for neigh, attrib in neighbors_first.items():
                    wn = attrib['weight']
                    if "miR" in neigh:
                        link = (n1, neigh, wn)
                        mirna_weights += 1
                        to_add_links.append(link)

                neighbors_first = full_graph[n2]
                for neigh, attrib in neighbors_first.items():
                    wn = attrib['weight']
                    if "miR" in neigh:
                        link = (n1, neigh, wn)
                        mirna_weights += 1
                        to_add_links.append(link)

            dataset_rows = to_add_links

            dfx2 = pd.DataFrame(dataset_rows)
            if dfx2.shape[0] > 0:
                new_df = []
                dfx2.columns = ['Interactor A', 'Interactor B', 'weight']
                for idx, row in dfx2.iterrows():
                    i1 = row['Interactor A']
                    i2 = row['Interactor B']
                    w = row['weight']

                    if i1 in gmap:
                        description_first = gmap[i1][0]
                        description_first2 = gmap[i1][1]

                    else:
                        description_first = "na"
                        description_first2 = "na"

                    if i2 in gmap:
                        description_second = gmap[i2][0]
                        description_second2 = gmap[i2][1]

                    else:
                        description_second = "na"
                        description_second2 = "na"

                    row = [
                        i1, i2, w, description_first, description_first2,
                        description_second, description_second2
                    ]
                    new_df.append(row)

                new_df2 = pd.DataFrame(new_df)
                new_df2.columns = [
                    'Interactor A', 'Interactor B', 'weight', 'Description A',
                    'Bin A', 'Description B', 'Bin B'
                ]

                if mirna_weights > 0:
                    print(f"Writing {cid} ..")
                    new_df2.to_csv(
                        "../communities_BN_for_cytoscape/{}_miR_{}.tsv".format(
                            cid, mirna_weights),
                        sep="\t",
                        index=False)

import glob
dpx = defaultdict(list)
for flx in glob.glob("../communities_BN_for_cytoscape/*"):
    if "miR" in flx:
        profile = flx.split("/")[-1].replace(".tsv", "")
        parts = profile.split("_")
        month = parts[1]
        pheno = parts[2]
        tag = month + "_" + pheno
        dpx[tag].append(flx)

for k, v in dpx.items():
    all_data = []
    for filex in v:
        actual_data = pd.read_csv(filex, sep="\t")
        all_data.append(actual_data)
    all_data = pd.concat(all_data)
    all_data.to_csv("../separate_BN_community_aggregates/{}.tsv".format(k),
                    sep="\t")
