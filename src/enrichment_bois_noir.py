### enrich BN data.
from prepare_basic_expression import add_annotations_enrichment, community_dissipation, community_dissipation_vertical, compare_pairwise_expressions, convert_to_gaf, cw, descriptionParser, export_communities_to_obj, fet_enrichment, get_expression_thresholds_by_time, get_measure_network_pd, graphParser, logging, merge_mrna_expressions, parse_LKN, pd, plot_networks, read_mrna_expression, read_srna_expression, return_temporal_dataframes, summarise_communities, visualize_dissipations
import sys
"""
The data for this step has been made available as part of the paper (see the repositories linked in the paper).
The annotation data is to be put in the ./data folder and the data_BN_clean should contain expression data. See the examples below.
"""


def read_mrna_normalized(fpath):
    nst = {}
    cnt = 0
    with open(fpath) as fp:
        for line in fp:
            cnt += 1
            if cnt > 2:
                line = line.strip().split("\t")
                node_name = line[0]
                features = [float(x) for x in line[3:19]]
                assert len(features) == 16
                nst[node_name] = {"normalized_expressions": features}
    return nst


if __name__ == "__main__":

    # comm -12 <(cat list1.txt| sort) <(cat list2.txt|sort)
    # ground_truth_network.csv  mRNA_normalized.csv  mRNA_raw_counts.csv  sRNA_expression.csv

    datafolder = "../data_BN_clean"
    mrna_expression = datafolder + "/mRNA_raw_counts.csv"
    mrna_normalized = datafolder + "/mRNA_normalized.csv"
    srna_expression = datafolder + "/sRNA_expression.csv"
    ground_truth_network = datafolder + "/ground_truth_network.csv"
    descriptions_file = "../data/vvi_descriptions_ath-sly-stu-sprot_merged.txt"

    ## FIRST OF ALL -> CONSTRUCT mRNA TABLE WITH NORMALIZED INFO
    mrna_expression_obj = read_mrna_expression(mrna_expression)
    mrna_normalized_obj = read_mrna_normalized(mrna_normalized)
    srna_expression_obj = read_srna_expression(srna_expression)
    mrna_expression_obj = merge_mrna_expressions(mrna_normalized_obj,
                                                 mrna_expression_obj)
    ground_truth_network = graphParser(ground_truth_network)
    description_map = descriptionParser(descriptions_file)
    logging.info("Found {} descriptions.".format(len(description_map)))

    ## to sta prosta parametra za dolociti omrezja!
    detection_algorithms = ["infomap", "louvain"]
    ontology_mapping = "../data/ontology.txt"

    ###################################################################
    ## Step 2: construct co-expression networks between sRNA and mRNA mols
    merged_expression = {**srna_expression_obj, **mrna_expression_obj}
    percentile_threshold = 20
    expression_thresholds = get_expression_thresholds_by_time(
        merged_expression, percentile_threshold)

    print("Parsing LKN ..")
    LKN_graph = parse_LKN("../data/LKN.txt")

    ## TAKE 1 - compare community sizes in time.
    ####################################################################
    ## get dataframes for individual time points

    h_june, p_june, h_sep, p_sep = return_temporal_dataframes(
        merged_expression,
        measure="expression",
        exp_lower_bound=expression_thresholds)
    h_june.to_csv("../result_community_BN/hjune.tsv", sep="\t")
    p_june.to_csv("../result_community_BN/pjune.tsv", sep="\t")
    h_sep.to_csv("../result_community_BN/hsep.tsv", sep="\t")
    p_sep.to_csv("../result_community_BN/psep.tsv", sep="\t")
    sys.exit()
    print("Got the dataframes ..")
    all_names = []
    all_names.append(set(h_june.columns))
    all_names.append(set(p_june.columns))
    all_names.append(set(h_sep.columns))
    all_names.append(set(p_sep.columns))
    common_names = set.intersection(*all_names)

    common_june_h = h_june[common_names]
    common_june_p = p_june[common_names]
    common_sep_h = h_sep[common_names]
    common_sep_p = p_sep[common_names]

    print("Inducing networks ..")
    network_june_h = get_measure_network_pd(h_june, ctype="both")
    network_june_p = get_measure_network_pd(p_june, ctype="both")
    network_sep_h = get_measure_network_pd(h_sep, ctype="both")
    network_sep_p = get_measure_network_pd(p_sep, ctype="both")

    # pg1 = nx.pagerank(nx.Graph(network_june_h.core_network))
    # pg2 = nx.pagerank(nx.Graph(network_june_p.core_network))
    # pg3 = nx.pagerank(nx.Graph(network_sep_h.core_network))
    # pg4 = nx.pagerank(nx.Graph(network_sep_p.core_network))

    network_june_h.save_network(
        output_file="../result_community_BN/network_june_h.gpickle",
        output_type="gpickle")
    network_june_p.save_network(
        output_file="../result_community_BN/network_june_p.gpickle",
        output_type="gpickle")
    network_sep_h.save_network(
        output_file="../result_community_BN/network_sep_h.gpickle",
        output_type="gpickle")
    network_sep_p.save_network(
        output_file="../result_community_BN/network_sep_p.gpickle",
        output_type="gpickle")

    ## prepare candidate networks
    #network_raw = get_measure_network_pd(h_june,ctype="both")
    LKN_graph.merge_with(ground_truth_network)
    #LKN_graph.merge_with(network_raw)
    LKN_graph.save_network(output_file="../result_community_BN/LKN.gpickle",
                           output_type="gpickle")

    try:
        comparisons_df = compare_pairwise_expressions(LKN_graph,
                                                      merged_expression,
                                                      mapping=description_map)
        comparisons_df = comparisons_df.sort_values(by=["miRNA activity"],
                                                    ascending=False)
        comparisons_df.to_csv("../result_community_BN/pairwise_mirna.tsv",
                              header=True,
                              sep="\t",
                              index=False)
        logging.info(comparisons_df)

        plot_networks(
            [network_june_h, network_june_p, network_sep_h, network_sep_p])
        logging.info("Finished plotting.")
    except Exception as es:
        print(es)

    for detection_algorithm in detection_algorithms:
        if detection_algorithm != "louvain":
            infomap_binary = "../bin/Infomap"
            j_h = cw.infomap_communities(network_june_h,
                                         binary=infomap_binary,
                                         iterations=500)
            j_p = cw.infomap_communities(network_june_p,
                                         binary=infomap_binary,
                                         iterations=500)
            s_h = cw.infomap_communities(network_sep_h,
                                         binary=infomap_binary,
                                         iterations=500)
            s_p = cw.infomap_communities(network_sep_p,
                                         binary=infomap_binary,
                                         iterations=500)
        else:
            j_h = cw.louvain_communities(network_june_h)
            j_p = cw.louvain_communities(network_june_p)
            s_h = cw.louvain_communities(network_sep_h)
            s_p = cw.louvain_communities(network_sep_p)

        dissipations_overall = community_dissipation(
            j_h, j_p, s_h, s_p,
            mapping=description_map).sort_values(by=['dissipation'],
                                                 ascending=False)

        dissipations_overall.to_csv(
            "../result_community_BN/horizontaldissipations_" +
            detection_algorithm + ".tsv",
            sep="\t")
        dissipations_overall = pd.read_csv(
            "../result_community_BN/horizontaldissipations_" +
            detection_algorithm + ".tsv",
            sep="\t")

        dissipations_overall_vertical = community_dissipation_vertical(
            j_h, j_p, s_h, s_p,
            mapping=description_map).sort_values(by=['dissipation'],
                                                 ascending=False)

        dissipations_overall_vertical.to_csv(
            "../result_community_BN/verticaldissipations_" +
            detection_algorithm + ".tsv",
            sep="\t")
        dissipations_overall_vertical = pd.read_csv(
            "../result_community_BN/verticaldissipations_" +
            detection_algorithm + ".tsv",
            sep="\t")

        visualize_dissipations(dissipations_overall, detection_algorithm)
        visualize_dissipations(dissipations_overall_vertical,
                               detection_algorithm + "_vertical")

        summarise_communities(j_h, name="j_h")
        summarise_communities(j_p, name="j_p")
        summarise_communities(s_h, name="s_h")
        summarise_communities(s_p, name="s_p")

        ###############
        ## Write communities for CBSSD analysis
        ###############

        out_obj = export_communities_to_obj([j_h, j_p, s_h, s_p],
                                            names=["J_H", "J_I", "S_H", "S_I"])
        out_obj.to_csv("../result_community_BN/ALL_" + detection_algorithm +
                       "_COMMUNITIES.tsv",
                       sep="\t")

        ## convert custom ontology to GAF format, suitable for CBSSD
        gaf_obj, annotations = convert_to_gaf(ontology_mapping)
        fx = open("../BK/gmm.gaf", "w+")
        fx.write("\n".join(gaf_obj))
        fx.close()
        whole_enrichments = []
        for alternative in ["greater"]:

            logging.info("Using alternative: {}".format(alternative))
            enrich_spring_h = fet_enrichment(j_h,
                                             gaf_file="../BK/gmm.gaf",
                                             annotation_file=annotations,
                                             name="J_H",
                                             alternative=alternative)
            enrich_spring_p = fet_enrichment(j_p,
                                             gaf_file="../BK/gmm.gaf",
                                             annotation_file=annotations,
                                             name="J_I",
                                             alternative=alternative)
            enrich_autumn_h = fet_enrichment(s_h,
                                             gaf_file="../BK/gmm.gaf",
                                             annotation_file=annotations,
                                             name="S_H",
                                             alternative=alternative)
            enrich_autumn_p = fet_enrichment(s_p,
                                             gaf_file="../BK/gmm.gaf",
                                             annotation_file=annotations,
                                             name="S_I",
                                             alternative=alternative)

            ## merge with community obj
            enrich_spring_h = pd.merge(enrich_spring_h, out_obj, on="id")
            enrich_spring_p = pd.merge(enrich_spring_p, out_obj, on="id")
            enrich_autumn_h = pd.merge(enrich_autumn_h, out_obj, on="id")
            enrich_autumn_p = pd.merge(enrich_autumn_p, out_obj, on="id")

            enrich_spring_h = add_annotations_enrichment(
                enrich_spring_h, description_map)
            enrich_spring_p = add_annotations_enrichment(
                enrich_spring_p, description_map)
            enrich_autumn_h = add_annotations_enrichment(
                enrich_autumn_h, description_map)
            enrich_autumn_p = add_annotations_enrichment(
                enrich_autumn_p, description_map)

            enrich_spring_h.to_csv(
                "../result_community_BN/individual_enrichment_" +
                detection_algorithm + "_enrich_spring_h_" + alternative +
                ".tsv",
                sep="\t")
            enrich_spring_p.to_csv(
                "../result_community_BN/individual_enrichment_" +
                detection_algorithm + "_enrich_spring_p_" + alternative +
                ".tsv",
                sep="\t")
            enrich_autumn_h.to_csv(
                "../result_community_BN/individual_enrichment_" +
                detection_algorithm + "_enrich_autumn_h_" + alternative +
                ".tsv",
                sep="\t")
            enrich_autumn_p.to_csv(
                "../result_community_BN/individual_enrichment_" +
                detection_algorithm + "_enrich_autumn_p_" + alternative +
                ".tsv",
                sep="\t")

            # whole_enrichments.append(enrich_spring_h)
            # whole_enrichments.append(enrich_spring_p)
            # whole_enrichments.append(enrich_autumn_h)
            # whole_enrichments.append(enrich_autumn_p)

        # whole_df = pd.concat(whole_enrichments)
        # whole_df.to_csv("../result_community_BN/ALL_ENRICHMENT.tsv",sep="\t")
        # whole_df = pd.read_csv("../result_community_BN/ALL_ENRICHMENT.tsv",sep="\t")

        # CBSSD_enrichment([s_h,s_p],
        #                  "../BK/gmm.gaf",
        #                  dataset_name="../semantic_datasets/example_partition_"+detection_algorithm+"_inputs.n3",
        #                  layer_type=None,
        #                  obo_ontology="../data/ontology.obo.gz",names=["s_h","s_p"],
        #                  output_rules = "../result_community_BN/hedwig_"+detection_algorithm+"_autumn)run.json")

        # CBSSD_enrichment([j_h,j_p],
        #                  "../BK/gmm.gaf",
        #                  dataset_name="../semantic_datasets/example_partition_"+detection_algorithm+"_inputs.n3",
        #                  layer_type=None,
        #                  obo_ontology="../data/ontology.obo.gz",names=["j_h","j_p"],
        #                  output_rules = "../result_community_BN/hedwig_"+detection_algorithm+"_spring_run.json")
