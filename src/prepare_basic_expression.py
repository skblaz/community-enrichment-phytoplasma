### add normalized counts to nodes.
from collections import Counter
import matplotlib.pyplot as plt
import seaborn as sns
from py3plex.algorithms.community_detection import community_wrapper as cw
from py3plex.visualization.multilayer import *
from py3plex.visualization.colors import colors_default
from py3plex.wrappers import train_node2vec_embedding
#from py3plex.visualization.embedding_visualization import embedding_visualization,embedding_tools
import numpy as np
from methods import *

from py3plex.algorithms.statistics.topology import *
from py3plex.core import multinet

import logging
logging.basicConfig(format='%(asctime)s - %(message)s',
                    datefmt='%d-%b-%y %H:%M:%S')
logging.getLogger().setLevel(logging.INFO)


def graphParser(fpath, tag="default"):

    A = multinet.multi_layer_network()

    cnt = 0
    with open(fpath) as fp:
        for line in fp:
            cnt += 1
            if cnt > 2:
                n1, interaction, n2 = line.strip().split("\t")
                source_type = get_node_type(n1)
                target_type = get_node_type(n2)
                simple_edge = {
                    "source": n1,
                    "target": n2,
                    "type": interaction + "_alignment",
                    "source_type": source_type,
                    "target_type": target_type
                }
                A.add_edges(simple_edge)
    A.basic_stats()
    return A


def rawTableParser(fpath, normalized_dict):
    """
    Take other features, such as fold change and sequence
    """

    nst = {}
    cnt = 0
    with open(fpath) as fp:
        for line in fp:
            cnt += 1
            if cnt > 2:
                line = line.strip().split("\t")
                node_name = line[0]
                DNA_seq = line[1]
                FC = [float(x) for x in line[2:6]]
                counts = [float(x) for x in line[10:]]
                nst[node_name] = {}
                nst[node_name]["DNA"] = DNA_seq
                nst[node_name]["FC"] = FC
                nst[node_name]["CNT"] = counts

    ## TODO - merge this with normalized dict
    set(normalized_dict.keys())
    set(nst.keys())
    return nst


def tableParser(fpath):

    nst = {}
    cnt = 0
    with open(fpath) as fp:
        for line in fp:
            cnt += 1
            if cnt > 2:
                line = line.strip().split("\t")
                node_name = line[0]
                chromosome = [float(line[1].replace("chr",
                                                    ""))]  ## to morda ni ok
                features = [float(x) for x in line[3:]]
                features + chromosome
                nst[node_name] = {
                    "normalized_expressions": features,
                    'chromosome': chromosome
                }

    return nst


def basic_network_stuff(network):

    partition = cw.infomap_communities(
        network,
        binary="/home/blazs/Py3plex/bin/Infomap",
        multiplex=False,
        verbose=True,
        iterations=200)
    ## select top n communities by size
    top_n = 5
    partition_counts = dict(Counter(partition.values()))
    top_n_communities = list(partition_counts.keys())[0:top_n]

    ## assign node colors
    color_mappings = dict(
        zip(top_n_communities,
            [x for x in colors_default if x != "black"][0:top_n]))

    network_colors = [
        color_mappings[partition[x]]
        if partition[x] in top_n_communities else "black"
        for x in network.get_nodes()
    ]

    ## visualize the network's communities!
    #hairball_plot(network.core_network,color_list = network_colors,layered=False,layout_parameters={"iterations" : 10},scale_by_size=True,layout_algorithm="force",legend=False)

    f = plt.figure()
    f = network.visualize_network()  ## visualize
    #f.savefig("../result_images/multilayer.png", bbox_inches='tight')
    network.save_network("./test.edgelist")
    plt.show()
    ## call a specific n2v compiled binary
    train_node2vec_embedding.call_node2vec_binary(
        "./test.edgelist",
        "./test_embedding.emb",
        binary="/home/blazs/Py3Plex/bin/node2vec",
        weighted=False)

    ## preprocess and check embedding
    network.load_embedding("./test_embedding.emb")
    output_positions = embedding_tools.get_2d_coordinates_tsne(
        network, output_format="pos_dict")

    ## custom layouts are part of the custom coordinate option
    layout_parameters = {}
    layout_parameters['pos'] = output_positions  ## assign parameters
    network_colors, graph = network.get_layers(style="hairball")

    f = plt.figure()
    f = hairball_plot(graph,
                      network_colors,
                      layout_algorithm="custom_coordinates",
                      layout_parameters=layout_parameters)
    #f.savefig("../result_images/hairball.png", bbox_inches='tight')
    plt.show()


def plot_correlation_matrix(df):

    correlations = df.corr()
    #    f, ax = plt.subplots(figsize=(10, 8))
    #sns.heatmap(correlations, mask=np.zeros_like(correlations, dtype=np.bool), cmap=sns.diverging_palette(220, 10, as_cmap=True),square=True, ax=ax,yticklabels=False,xticklabels=False)

    #plt.show()
    f = plt.figure()
    f = sns.clustermap(correlations,
                       mask=np.zeros_like(correlations, dtype=np.bool),
                       cmap=sns.diverging_palette(220, 10, as_cmap=True),
                       square=True,
                       yticklabels=False,
                       xticklabels=False)
    f.savefig("../result_images/clustermap.png", bbox_inches='tight')
    #plt.show()


def plot_community_partition(partition, network):

    ## select top n communities by size
    top_n = 15
    partition_counts = dict(Counter(partition.values()))
    top_n_communities = list(partition_counts.keys())[0:top_n]

    ## assign node colors
    color_mappings = dict(
        zip(top_n_communities,
            [x for x in colors_default if x != "black"][0:top_n]))

    network_colors = [
        color_mappings[partition[x]]
        if partition[x] in top_n_communities else "black"
        for x in network.get_nodes()
    ]

    ## visualize the network's communities!
    hairball_plot(network.core_network,
                  color_list=network_colors,
                  layered=False,
                  layout_parameters={"iterations": 100},
                  scale_by_size=True,
                  layout_algorithm="force",
                  legend=False)
    plt.show()


def plot_community_pivot(pdfm):

    sns.heatmap(pdfm,
                mask=np.zeros_like(pdfm, dtype=np.bool),
                square=True,
                yticklabels=False,
                xticklabels=False)
    plt.xlabel("Communities known network")
    plt.ylabel("Communities expression")
    plt.show()


def plot_community_freq(pdfm):

    print(pdfm.head())
    x = np.array(pdfm['w'].sort_values(ascending=False))
    plt.plot(x, color="black")
    plt.xlabel("Intersection index.")
    plt.ylabel("Number of common RNAs.")
    plt.show()


def plot_power_law_px(network):

    degree_vector = network.get_degrees()
    val_vect = np.array(list(degree_vector.values()))
    print(val_vect)
    print("Plotting degree distribution..")
    plot_power_law(val_vect, "", "Node degree", "individual node")
    plt.show()


def compare_community_sizes(d1, d2, d3, d4):

    size_d1 = Counter(list(d1.values()))
    size_d2 = Counter(list(d2.values()))
    size_d3 = Counter(list(d3.values()))
    size_d4 = Counter(list(d4.values()))

    s1 = np.flip(np.sort(np.array(list(dict(size_d1).values()))))
    s2 = np.flip(np.sort(np.array(list(dict(size_d2).values()))))
    s3 = np.flip(np.sort(np.array(list(dict(size_d3).values()))))
    s4 = np.flip(np.sort(np.array(list(dict(size_d4).values()))))

    plt.plot(s1, label="June - healthy")
    plt.plot(s2, label="June - infected")
    plt.plot(s3, label="Sep - healthy")
    plt.plot(s4, label="Sep - infected")
    #    plt.yscale('log')
    plt.legend()
    plt.show()

    diff_h = np.max(s3) - np.max(s1)
    diff_p = np.max(s4) - np.max(s2)
    print(diff_h, diff_p)


def tableParser_sRNA_normalized(fpath):

    nst = {}
    cnt = 0
    with open(fpath) as fp:
        for line in fp:
            cnt += 1
            if cnt > 2:
                line = line.strip().split("\t")
                node_name = line[0]
                chromosome = [float(line[1].replace("chr",
                                                    ""))]  ## to morda ni ok
                features = [float(x) for x in line[3:]]
                features + chromosome
                nst[node_name] = {
                    "normalized_expressions": features,
                    'chromosome': chromosome
                }

    return nst


def tableParser_mRNA_raw(fpath):

    nst = {}
    cnt = 0
    with open(fpath) as fp:
        for line in fp:
            cnt += 1
            if cnt > 2:
                line = line.strip().split("\t")
                node_name = line[0]
                chromosome = [float(line[1].replace("chr",
                                                    ""))]  ## to morda ni ok
                features = [float(x) for x in line[3:]]
                features + chromosome
                nst[node_name] = {
                    "normalized_expressions": features,
                    'chromosome': chromosome
                }

    return nst


def read_mrna_expression(fpath):

    nst = {}
    cnt = 0
    with open(fpath) as fp:
        for line in fp:
            cnt += 1
            if cnt > 2:
                line = line.strip().split("\t")
                node_name = line[0]
                features = [float(x) for x in line[10:26]]
                description = line[-1]
                nst[node_name] = {
                    "normalized_expressions": np.log(features),
                    "description": description
                }
    return nst


def read_srna_expression(fpath):
    nst = {}
    cnt = 0
    with open(fpath) as fp:
        for line in fp:
            cnt += 1
            if cnt > 2:
                line = line.strip().split("\t")
                node_name = line[0]
                features = [float(x) for x in line[10:]]
                assert len(features) == 16
                nst[node_name] = {"normalized_expressions": np.log(features)}
    return nst


def merge_mrna_expressions(norm, raw):
    nd = {}
    for k, v in raw.items():
        try:
            nd[k] = {
                "normalized_expressions": norm[k]['normalized_expressions']
            }


#            assert nd[k]['normalized_expressions'] == norm[k]['normalized_expressions']
        except Exception as es:
            print(es, "not cool.")
    return nd


def visualize_distribution(dat, title="test"):

    vals = [x['lend'] for x in dat]
    sns.distplot(vals)
    plt.xlabel("Alignment size")
    plt.ylabel("Density")
    plt.title(title)
    plt.show()


def plot_distances(dfx, name="test"):
    print(dfx.head())
    f = plt.figure()
    data = dfx.pivot('com1', 'com2', 'jaccard')
    sns.heatmap(data)
    f.savefig("../result_images/jaccard_distr" + name + "png",
              bbox_inches='tight')


def visualize_dissipations(pdx, name):

    try:
        f = plt.figure()
        pdx1 = pdx[pdx.time == "autumn"]
        sns.distplot(pdx1.dissipation, label="Dissipation autumn")

        pdx2 = pdx[pdx.time == "spring"]
        sns.distplot(pdx2.dissipation, label="Dissipation spring")

        plt.xlabel("Dissipation amount")
        plt.ylabel("Number of communities")
        plt.legend()
        f.savefig("../result_images/dissipation_" + name + ".png",
                  bbox_inches='tight',
                  dpi=300)
    except:
        pass


def plot_networks(list_networks=None):

    weights_mRNA = []
    weights_sRNA = []
    try:
        for network in list_networks:
            for edge in network.get_edges(data=True):
                if edge[1][1] == "mRNA" and edge[0][1] == "mRNA":
                    weights_mRNA.append(edge[2]['weight'])
                else:
                    weights_sRNA.append(edge[2]['weight'])

        f = plt.figure()
        sns.distplot(weights_mRNA, label="mRNA weights")
        sns.distplot(weights_sRNA, label="sRNA weights")
        plt.legend()
        plt.xlabel("Covariance of an edge")
        plt.ylabel("Density estimation")
        f.savefig("../result_images/distributions.png",
                  bbox_inches='tight',
                  dpi=300)
    except:
        pass
