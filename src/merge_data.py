import pandas as pd
import numpy as np
import glob
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--result_folder', default="../result_community_DR")
args = parser.parse_args()

comfol = args.result_folder + "/"
flx = glob.glob(comfol + "/*")
vertical_dissipations = comfol + "verticaldissipations_infomap.tsv"
horizontal_dissipations = comfol + "horizontaldissipations_infomap.tsv"

dissipations_horizontal = pd.read_csv(
    horizontal_dissipations, sep="\t")[['name', 'direction', 'dissipation']]
dissipations_vertical = pd.read_csv(
    vertical_dissipations, sep="\t")[['name', 'direction', 'dissipation']]

dissipations_horizontal['dissipation'] = np.abs(
    dissipations_horizontal['dissipation'])
dissipations_vertical['dissipation'] = np.abs(
    dissipations_vertical['dissipation'])

dissipations_horizontal['dissipation'] = (
    dissipations_horizontal['dissipation'] -
    dissipations_horizontal['dissipation'].min()) / (
        dissipations_horizontal['dissipation'].max() -
        dissipations_horizontal['dissipation'].min())

dissipations_vertical['dissipation'] = (
    dissipations_vertical['dissipation'] -
    dissipations_vertical['dissipation'].min()) / (
        dissipations_vertical['dissipation'].max() -
        dissipations_vertical['dissipation'].min())

# plt.plot(dissipations_vertical.dissipation, label = "Vertical")
# plt.plot(dissipations_horizontal.dissipation, label = "Horizontal")
# plt.legend()
# plt.xlabel("Consequent community member")
# plt.ylabel("Dissipation")
# plt.show()

#total_dissipations = pd.concat([dissipations_horizontal,dissipations_vertical])
## merge individual files as follows

percentile = 0.85
pvalue = 0.05
for fl_name in flx:
    if "individual_enrichment" in fl_name:
        nmx = "_".join(fl_name.split("/")[2].split(".")[0].split("_")[-3:-1])
        fnx = pd.read_csv(fl_name, sep="\t")
        fnx = fnx.groupby(by='id').filter(lambda x: len(x) > 5 and len(x) < 30)
        fnx = fnx.groupby(by='descriptions').filter(lambda x: len(x) > 5)

        merged_dfx_horizontal = pd.merge(fnx.drop_duplicates(),
                                         dissipations_horizontal,
                                         left_on="id",
                                         right_on="name")

        merged_dfx_horizontal = merged_dfx_horizontal[
            (merged_dfx_horizontal.dissipation > 0)
            & (merged_dfx_horizontal.pval < pvalue)]

        qh1 = merged_dfx_horizontal['dissipation'].quantile(percentile)

        merged_dfx_horizontal[
            'in top 85%'] = merged_dfx_horizontal['dissipation'] > qh1

        merged_dfx_horizontal.drop_duplicates().to_csv(
            comfol + "DE_horizontal_" + nmx + ".tsv", sep="\t")

        merged_dfx_vertical = pd.merge(fnx.drop_duplicates(),
                                       dissipations_vertical,
                                       left_on="id",
                                       right_on="name")

        merged_dfx_vertical = merged_dfx_vertical[
            (merged_dfx_vertical.dissipation > 0)
            & (merged_dfx_vertical.pval < pvalue)]

        qv1 = merged_dfx_vertical['dissipation'].quantile(percentile)
        merged_dfx_vertical[
            'in top 85%'] = merged_dfx_vertical['dissipation'] > qv1

        merged_dfx_vertical.drop_duplicates().to_csv(comfol + "DE_vertical_" +
                                                     nmx + ".tsv",
                                                     sep="\t")
