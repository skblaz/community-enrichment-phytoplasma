def get_measure_network_pd(pdf,
                           threshold=1,
                           ctype="x",
                           measure="corr",
                           scale_free_test=True,
                           upper_bound_dist=10):

    current_min_alpha = 0
    current_max_alpha = 10000000

    if type(threshold) == float:
        threshold = [threshold]
    else:
        pass

    all_edges = []
    pdf.columns.values

    print("Dataframe shape:", pdf.shape)
    distances = pdf.corr('spearman')
    parsed = set()
    for emx1, a in enumerate(distances.columns):
        for emx2, b in enumerate(distances.columns):
            if not (a, b) in parsed and not (b, a) in parsed:
                weight_val = distances.iloc[emx1, emx2]
                all_edges.append({
                    "source": a,
                    "target": b,
                    "type": "empirical",
                    "source_type": get_node_type(a),
                    "target_type": get_node_type(b),
                    "weight": weight_val
                })
                parsed.add((a, b))

    print("Starting evaluation ..")
    all_edge_df = pd.DataFrame(all_edges)
    if scale_free_test:
        for thr in threshold:
            print("Testing threshold {}".format(thr))
            subset = all_edge_df.loc[all_edge_df['weight'] <= thr]
            final_edges = subset.to_dict('records')
            print(len(final_edges))
            Corr_net = multinet.multi_layer_network(directed=False)
            try:
                Corr_net.add_edges(final_edges)
            except Exception as es:
                print(es)
                continue
            Corr_net.basic_stats()
            alpha, sigma = Corr_net.test_scale_free()
            if alpha > 2 and alpha < 3:
                current_optimum_network = Corr_net
                print("OPTIMAL ALPHA: {}".format(alpha))
                return current_optimum_network

            elif alpha > current_min_alpha and alpha < current_max_alpha:
                print("OPTIMUM alpha {}".format(alpha))
                current_optimum_network = Corr_net
                del Corr_net

        return current_optimum_network
