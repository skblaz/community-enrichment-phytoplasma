## merge with lkn
import pandas as pd
import tqdm

#import SCD

import networkx as nx

ontofile = pd.read_csv('../data/ontology.txt', sep="\t")
ontofile.columns = ['bin', 'description', 'gene', 'gene2']
gmap = {}
for idx, row in ontofile.iterrows():
    gid = row['gene']
    description = row['description']
    rb = row['bin']
    gmap[gid] = [description, rb]
lkn = "LKN.gpickle"
network_files = [
    "network_june_h.gpickle", "network_june_p.gpickle",
    "network_sep_h.gpickle", "network_sep_p.gpickle"
]

print("Reading graphs.")


def merge_with_lkn(lkn_graph, some_graph, gmap):

    fdf = []
    kg_links = dict()
    for edge2 in some_graph.edges(data=True):
        n3, n4 = edge2[0][0], edge2[1][0]
        e2w = edge2[2]['weight']
        kg_links[(n3, n4)] = e2w
        kg_links[(n4, n3)] = e2w

    for edge in tqdm.tqdm(lkn_graph.edges(data=True)):
        n1, n2 = edge[0][0], edge[1][0]
        e1t = edge[2]['type']
        if (n1, n2) in kg_links:
            weight = kg_links[(n1, n2)]
            if n1 in gmap:
                desc = gmap[n1][0]
                binx = gmap[n1][1]

            else:
                desc = "na"
                binx = "na"

            if n2 in gmap:
                desc1 = gmap[n2][0]
                binx1 = gmap[n2][1]

            else:
                desc1 = "na"
                binx1 = "na"

            fdf.append((n1, n2, weight, e1t, desc, binx, desc1, binx1))
    dfx = pd.DataFrame(fdf)
    dfx.columns = [
        'Interactor 1', 'Interactor 2', 'score', 'interaction type',
        'description p1', 'bin p1', 'description p2', 'bin p2'
    ]
    return dfx


LKN_graph = nx.read_gpickle(
    "../result_community_BN/{}".format(lkn)).to_undirected()

for gfile in network_files:
    graph = nx.read_gpickle(
        "../result_community_BN/{}".format(gfile)).to_undirected()
    isolated = list(nx.isolates(graph))
    graph.remove_nodes_from(list(nx.isolates(graph)))
    gname = "_".join(gfile.replace(".gpickle", "").split("_")[1:])
    print("parsing", gname)
    fdf2 = merge_with_lkn(LKN_graph, graph, gmap)
    fdf2.to_csv("../interactions/MAJA_AND_{}.tsv".format(gname), sep="\t")
