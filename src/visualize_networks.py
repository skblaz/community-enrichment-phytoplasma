from py3plex.algorithms.community_detection import community_wrapper as cw
from py3plex.core import multinet
from py3plex.visualization.multilayer import hairball_plot, plt
from py3plex.visualization.colors import colors_default
from collections import Counter

#import SCD
from collections import Counter

import networkx as nx

graph = nx.read_gpickle(
    "../result_community_BN/network_sep_p.gpickle").to_undirected()
print(nx.info(graph))
isolated = list(nx.isolates(graph))
graph.remove_nodes_from(list(nx.isolates(graph)))

G = multinet.multi_layer_network()
G = nx.Graph()
for edge in graph.edges(data=True):
    ## add a single edge with type
    G.add_edge(edge[0], edge[1])
graph = G
#G.visualize_network(show=True)

partition = cw.louvain_communities(graph)
#partition = cw.infomap_communities(G

top_n = 3
partition_counts = dict(Counter(partition.values()))
top_n_communities = list(partition_counts.keys())[0:top_n]

# assign node colors
color_mappings = dict(
    zip(top_n_communities,
        [x for x in colors_default if x != "black"][0:top_n]))

network_colors = [
    color_mappings[partition[x]]
    if partition[x] in top_n_communities else "black" for x in graph.nodes()
]

# visualize the network's communities!
hairball_plot(graph,
              color_list=network_colors,
              layout_parameters={"iterations": 30},
              scale_by_size=True,
              layout_algorithm="force",
              legend=False)
plt.show()
