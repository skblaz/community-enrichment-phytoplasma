## Community enrichment and network reconstruction code
To reproduce the key steps, you can run:
```
cd src; bash run_all.sh
```
Additional data details and similar information can be found in the code documentation pages in each `.py` file. Large part of this code is currently being rewritten in the form of a simple-to-use library, you can contact the authors regarding the release.
The requirements are specified in:
```
src/requirements.txt
```
## System requirements:
1. RAM > 32 GB (graph get quite large)
2. At least 8CPUs, the community detection part runs in parallel (also enrichment).